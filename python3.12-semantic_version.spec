%global __python3 /usr/bin/python3.12
%global python3_pkgversion 3.12

%global pypi_name semantic_version
%global srcname python-semanticversion

Name:           python%{python3_pkgversion}-%{pypi_name}
Version:        2.10.0
Release:        2%{?dist}
Summary:        Library implementing the 'SemVer' scheme

License:        BSD
URL:            https://github.com/rbarrois/python-semanticversion
Source:         %{url}/archive/%{version}/%{srcname}-%{version}.tar.gz

BuildArch:      noarch
BuildRequires:  make
BuildRequires:  python%{python3_pkgversion}-devel
BuildRequires:  python%{python3_pkgversion}-rpm-macros
BuildRequires:  python%{python3_pkgversion}-setuptools
BuildRequires:  python%{python3_pkgversion}-pytest
BuildRequires:  sed

%global _description %{expand:
This small python library provides a few tools to handle semantic versioning in
Python.}

%description %{_description}

%prep
%autosetup -p1 -n %{srcname}-%{version}

# Drop unnecessary dependency
sed -i '/zest\.releaser\[recommended\]/d' setup.cfg

%build
%py3_build

%install
%py3_install

%check
%pytest

%files -n python%{python3_pkgversion}-%{pypi_name}
%license LICENSE
%doc README.rst ChangeLog CREDITS
%{python3_sitelib}/%{pypi_name}/
%{python3_sitelib}/%{pypi_name}-*.egg-info/

%changelog
* Tue Jan 23 2024 Miro Hrončok <mhroncok@redhat.com> - 2.10.0-2
- Rebuilt for timestamp .pyc invalidation mode

* Thu Oct 19 2023 Tomáš Hrnčiar <thrnciar@redhat.com> - 2.10.0-2
- Initial package
- Fedora contributions by:
      Davide Cavalca <dcavalca@fedoraproject.org>
      Dennis Gilmore <dennis@ausil.us>
      Haikel Guemar <hguemar@fedoraproject.org>
      Igor Gnatenko <ignatenkobrain@fedoraproject.org>
      Javier Pena <jpena@redhat.com>
      Miro Hrončok <miro@hroncok.cz>
      Petr Viktorin <pviktori@redhat.com>
